from matplotlib import pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np

def preprocessing(df):

    # Pretprocesiranje podataka
    from utils_nans1 import check_for_missing_values

    df.drop_duplicates(subset=None,inplace=True) # Uklanjanje duplikata
    result = check_for_missing_values(df)        # Provera nepostojecih vrednosti

    # Da bi koristili metode za resavanje nepostojecih vrednosti
    # napisao sam funkciju koja brise n% elemenata iz tabele
    if result.empty:
        from utils_nans1 import remove_percentage_of_data

        df = remove_percentage_of_data(df, "volatile acidity", 30)
        df = remove_percentage_of_data(df, "chlorides", 20)

        result = check_for_missing_values(df)

    print(df)
    # Interpolacija nepostojecih vrednosti -> prosekom
    for col in result.index.tolist():
        col_mean = df[col].mean()
        df[col] = df[col].fillna(col_mean)
        # df[col] = df[col].interpolate(method='spline', order=3, limit_direction='both')
        # df[col] = df[col].interpolate(method='spline', order=1, limit_direction='both')

    print(df)

def print_attribute_relationships(df):
    
    # Grafik odnosa izmedju kvaliteta vina za nase podatke
    sns.countplot(x='quality', data=df)
    plt.show()

    # Prikaz matrice korelacije
    correlation_matrix = df.corr()

    plt.figure(figsize=(8, 7))
    sns.heatmap(correlation_matrix, annot=True, cmap='coolwarm', fmt='.2f', linewidths=0.1)
    plt.title('Matrica korelacije')
    plt.show()

def check_assumptions(X_train, y_train):
    # Proveravamo da li su zadovoljene pretpostavke
    from utils_nans1 import are_assumptions_satisfied
    import statsmodels.api as sm

    x_sm = sm.add_constant(X_train)
    statsmodels_model = sm.OLS(y_train,x_sm).fit()

    print(are_assumptions_satisfied(statsmodels_model, x_sm, y_train))
    # Sve osim normalnosti su zadovoljene -> sve je zadovoljeno
    # Normalnost se ne racuna kada ima mnogo podataka u skupu

def split_dataset(df):
    from sklearn.model_selection import train_test_split

    X = df.drop('quality', axis='columns')
    y = df['quality']

    return train_test_split(X, y, train_size=0.8, shuffle=True, random_state=42)

def make_models(X_train,y_train):

    from sklearn.covariance import EllipticEnvelope
    from sklearn.linear_model import (
        RANSACRegressor,
        LinearRegression,
    )
    
    # LMS (Least Median Squares)
    lms = LinearRegression()
    lms.fit(X_train, y_train)

    # LTS (Least Trimmed Squares)
    lts = RANSACRegressor(min_samples=0.5)
    lts.fit(X_train, y_train)

    # RANSAC (Random Sample Consensus)
    ransac = RANSACRegressor()
    ransac.fit(X_train, y_train)

    # DB-Scan preprocesiranje
    outlier_detection = EllipticEnvelope(contamination=0.1)
    outlier_detection.fit(X_train)
    outliers = outlier_detection.predict(X_train)

    # Filtriranje outlinera
    X_train_filtered = X_train[outliers == 1]
    y_train_filtered = y_train[outliers == 1]

    # Ponovna treniranje modela na filtriranim podacima
    ols_after_dbscan = LinearRegression()
    ols_after_dbscan.fit(X_train_filtered, y_train_filtered)

    return [lms, lts, ransac, ols_after_dbscan]

def training_models(models, X_train, y_train, X_test, y_test):
    
    from sklearn.model_selection import cross_val_score

    # Inicijalizacija praznih lista za rezultate
    avg_mse_scores = []
    avg_r2_scores = []

    # Evaluacija svakog modela
    for model in models:
        mse_scores = cross_val_score(model, X_train, y_train, cv=5, scoring='neg_mean_squared_error')
        r2_scores = cross_val_score(model, X_train, y_train, cv=5, scoring='r2')

        avg_mse = -mse_scores.mean()
        avg_r2 = r2_scores.mean()

        avg_mse_scores.append(avg_mse)
        avg_r2_scores.append(avg_r2)
        
        print(f"{model.__class__.__name__} - Avg MSE: {avg_mse}, Avg R-squared: {avg_r2}")

    best_mse_model_index = avg_mse_scores.index(min(avg_mse_scores))
    best_r2_model_index = avg_r2_scores.index(max(avg_r2_scores))

    best_mse_model = models[best_mse_model_index]
    best_r2_model = models[best_r2_model_index]

    from utils_nans1 import evaluate_model
    # Ispis rezultata
    print("\nNajbolji MSE Model:")
    evaluate_model(y_test, best_mse_model.predict(X_test), "Najbolji MSE Model")

    print("\nNajbolji R-squared Model:")
    evaluate_model(y_test, best_r2_model.predict(X_test), "Najbolji R-squared Model")

    return remove_attributes(best_mse_model,X_train,y_train,X_test,y_test)

def remove_attributes(best_model, X_train, y_train, X_test, y_test):

    coefficients = pd.Series(best_model.coef_, index=X_train.columns)
    print("Koeficijenti:\n", coefficients)
    # Primjer: Ako apsolutna vrijednost koeficijenta manja od nekog praga (npr. 0.05), razmotrite brisanje te značajke
    threshold = 0.05
    attributes_to_drop = coefficients[abs(coefficients) < threshold].index
    print("Potencijalno nevažni atributi:\n", list(attributes_to_drop))

    X_train2 = X_train.drop(attributes_to_drop, axis=1)
    X_test2 = X_test.drop(attributes_to_drop, axis=1)

    model_new = best_model.fit(X_train2, y_train)

    coefficients = pd.Series(model_new.coef_, index=X_train2.columns)
    print("Koeficijenti:\n", coefficients)

    from utils_nans1 import evaluate_model
    print("\nMSE Model:")
    evaluate_model(y_test, model_new.predict(X_test2), "MSE Model")

    return model_new, X_train2, X_test2

def predction(best_model):

    columns = ["volatile acidity", "citric acid","chlorides", "density", "pH", "sulphates", "alcohol"]

    instance_values = [1.185,0,0.097,0.9966,3.63,0.54,10.7]
    instance_values2 = [1.185,0,0.015,0.9966,3.63,0.54,10.7]
    instance_values3 = [1.185,0,0.097,0.9966,3.63,0.54,15]

    new_instance = pd.DataFrame([instance_values], columns=columns)
    new_prediction = best_model.predict(new_instance)
    print("Predikcija za novi element: ", int(round(new_prediction[0],1)))

    new_instance = pd.DataFrame([instance_values2], columns=columns)
    new_prediction = best_model.predict(new_instance)
    print("Predikcija za novi element: ", int(round(new_prediction[0],1)))

    new_instance = pd.DataFrame([instance_values3], columns=columns)
    new_prediction = best_model.predict(new_instance)
    print("Predikcija za novi element: ", int(round(new_prediction[0],1)))

def print_addiction(df, columns):
    columns.append('quality')
    df2 = df[columns]
    correlation_with_quality = df2.corr()['quality'].abs().sort_values(ascending=False)

    plt.figure(figsize=(10, 6))
    sns.barplot(x=correlation_with_quality.values, y=correlation_with_quality.index, color='dodgerblue')
    plt.title('Apsolutne vrednosti korelacije sa kvalitetom vina')
    plt.xlabel('Apsolutna vrednost korelacije')
    plt.ylabel('Atributi')
    plt.show()

def main():
    # Učitavanje podataka
    url = "dataset/winequality-red.csv" 
    df = pd.read_csv(url,sep=';')
    print(df.head())

    preprocessing(df)

    X_train, X_test, y_train, y_test = split_dataset(df)
    check_assumptions(X_train,y_train)

    print_attribute_relationships(df)

    models = make_models(X_train,y_train)

    best_model, X_train, X_test = training_models(models,X_train,y_train,X_test,y_test)

    predction(best_model)

    print_addiction(df,list(X_train.columns))

if __name__ == "__main__":
    main()